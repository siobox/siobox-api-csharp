﻿using System;
using System.Runtime.InteropServices;
using SioBoxAPI.Addons;
using SioBoxAPI.Runtime.Engine;

namespace SioBoxAPI
{
    public static class Engine
    {

        [DllExport("TestFunction", CallingConvention = CallingConvention.StdCall)]
        public static void TestFunction()
        {

        }

        [DllExport("SioBoxAPI_INITIALIZE_ENGINE", CallingConvention = CallingConvention.StdCall)]
        public static void initialize_engine(ref bool InbSuccess)
        {
            try {
                AddonManager.initialize_engine();
                TimerManager.initialize_engine();
                InbSuccess = true;
            } catch (Exception e) {
                InbSuccess = false;
                Debug.engine_error(e);
            }
        }
        
        [DllExport("SioBoxAPI_DEINITIALIZE_ENGINE", CallingConvention = CallingConvention.StdCall)]
        public static void deinitialize_engine()
        {
            try {
                AddonManager.deinitialize_engine();
                TimerManager.deinitialize_engine();
            } catch (Exception e) {
                Debug.engine_error(e);
            }
        }

        public static bool IsX64 => IntPtr.Size == 8;
    }
}
