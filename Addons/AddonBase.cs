﻿namespace SioBoxAPI.Addons {
    public abstract class AddonBase
    {
        public event AddonLoadDelegate OnAddonLoad;
        public event AddonUnloadDelegate OnAddonUnload;
        public event AddonEnableDelegate OnAddonEnable;
        public event AddonDisableDelegate OnAddonDisable;
        public event AddonDependencyRequired OnAddonDependencyRequired;
        public event AddonDependencyDestroyed OnAddonDependencyDestroyed;
        public event AddonHotReload OnAddonHotReload;
        public event AddonHotReloaded OnAddonHotReloaded;

        public AddonInfo AddonInfo { get; internal set; }
        
        internal void LoadAddon()
        {
            OnAddonLoad?.Invoke();
        }

        internal void EnableAddon()
        {
            OnAddonEnable?.Invoke();
        }

        internal void DisableAddon()
        {
            OnAddonDisable?.Invoke();
        }

        internal void UnloadAddon()
        {
            OnAddonUnload?.Invoke();
        }

        internal void RequireDependency(AddonBase otherAddon)
        {
            OnAddonDependencyRequired?.Invoke(otherAddon);
        }

        internal void DestroyDependency(AddonBase otherAddon)
        {
            OnAddonDependencyDestroyed?.Invoke(otherAddon);
        }

        internal void HotReload() {
            OnAddonHotReload?.Invoke();
        }

        internal void HotReloaded() {
            OnAddonHotReloaded?.Invoke();
        }

        internal void init(AddonInfo addonInfo) {
            AddonInfo = addonInfo;
            LoadAddon();
        }
    }
}