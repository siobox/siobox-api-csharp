﻿namespace SioBoxAPI.Addons
{
    public class AddonState
    {
        public enum StateEnum {
            LOADED,
            ENABLED,
            DISABLED,
            UNLOADED,
            HOT_RELOADING,
        }

        public StateEnum State { get; private set; } = StateEnum.UNLOADED;

        public void SetLoaded() {
            
        }

        public void SetUnloaded() {
            
        }

        public void SetEnabled() {
            
        }

        public static void SetDisabled() {
            
        }
    }
}
