﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SioBoxAPI.Runtime.Core.Misc;

namespace SioBoxAPI.Addons
{

    public delegate void AddonLoadDelegate();
    public delegate void AddonUnloadDelegate();
    public delegate void AddonEnableDelegate();
    public delegate void AddonDisableDelegate();
    public delegate void AddonDependencyRequired(AddonBase otherAddon);
    public delegate void AddonDependencyDestroyed(AddonBase otherAddon);
    public delegate void AddonHotReload();
    public delegate void AddonHotReloaded();

    public struct AddonInfo {
        [JsonProperty("id", Required = Required.Always)] public string Id;
        [JsonProperty("name", Required = Required.Always)] public string Name;
        [JsonProperty("version", Required = Required.Always)] public string Version;
        [JsonProperty("description", Required = Required.AllowNull)] public string Description;
        [JsonProperty("dependencies", Required = Required.AllowNull)] public string[] Dependencies;
        public AddonState State;
        public string Path;
        public List<string> UnresolvedDependencies;
        public string AddonMainScriptPath;

        AddonInfo(string addonDirectoryPath) {
            string addonInfoPath = Paths.Combine(addonDirectoryPath, "addon.json");
            if(!File.Exists(addonInfoPath)) throw new FileNotFoundException(addonInfoPath);
            string text;
            try {
                text = File.ReadAllText(addonInfoPath);
            } catch (Exception e) {
                throw new AccessViolationException(e.Message, e.InnerException);
            }
            var parser = JObject.Parse(text);
            Id = parser["id"].ToString();
            Name = parser["name"].ToString();
            Version = parser["version"].ToString();
            Description = parser["description"]?.ToString();
            Dependencies = JsonConvert.DeserializeObject<List<string>>(parser["dependencies"]?.ToString()).ToArray();
            Path = addonDirectoryPath;
            AddonMainScriptPath = Paths.Combine(Paths.Combine(Path, "Scripts"), "Addon.cs");
            UnresolvedDependencies = new List<string>();
            State = new AddonState();
        }

        public AddonInfo[] GetDependencies(AddonInfo[] addons) {
            AddonInfo[] result = new AddonInfo[Dependencies.Length];
            for (int i = 0; i < result.Length; i++) {
                var addonId = Dependencies[i];
                try {
                    result[i] = addons.First(add => add.Id == addonId);
                } catch (Exception e) {
                    UnresolvedDependencies.Add(Dependencies[i]);
                }
            }
            return result;
        }

        public static ErrorContainer<AddonInfo> GetFromFile(string path) {
            try {
                return new AddonInfo(path).ToErrorContainer();
            } catch (FileNotFoundException e) {
                return ErrorContainer<AddonInfo>.FromError<AddonInfo>(new Error(message: $"Addon.json file was not found. [EnteredPath: {path}]"));
            } catch (AccessViolationException e) {
                return ErrorContainer<AddonInfo>.FromError<AddonInfo>(new Error(message: $"Addon.json could not be readed. [EnteredPath: {path}]"));
            } catch (JsonReaderException e) {
                return ErrorContainer<AddonInfo>.FromError<AddonInfo>(new Error(message: $"Addon.json looks like malformed. [EnteredPath: {path}]"));
            }
        }

        public override string ToString() {
            return $"{nameof(Id)}: {Id}, {nameof(Name)}: {Name}, {nameof(Version)}: {Version}, {nameof(Description)}: {Description}, {nameof(Dependencies)}: {Dependencies}, {nameof(State)}: {State}, {nameof(Path)}: {Path}, {nameof(UnresolvedDependencies)}: {UnresolvedDependencies}, {nameof(AddonMainScriptPath)}: {AddonMainScriptPath}";
        }
    }

}
