﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Policy;
using Microsoft.CSharp;
using SioBoxAPI.Runtime.Core.Misc;

namespace SioBoxAPI.Addons {
    public class AddonManager {
        private readonly Dictionary<AddonInfo, AddonBase> m_addons;
        private readonly CSharpCodeProvider m_cSharpCodeProvider;
        private static AddonManager m_instance;
        private static AppDomain m_appDomain;
        internal static void initialize_engine()
        {
            if (m_instance == null) //throw new EngineException("Trying to instantiate AddonManager when is already instantiated. This is not allowed!");
            {
                var addonManager = new AddonManager();

            }
        }

        internal static void deinitialize_engine()
        {
            if (m_instance != null) { //throw new EngineException("Trying to deinitialize AddonManager when is not initialized. This is not allowed!");
                foreach (var keyValuePair in m_instance.m_addons) {
                    keyValuePair.Value.DisableAddon();
                }

                foreach (var keyValuePair in m_instance.m_addons) {
                    keyValuePair.Value.UnloadAddon();
                }

                m_instance.UnloadPlugins();
                m_instance = null;
            }
        }

        private AddonManager() {
            m_instance = this;
            m_addons = new Dictionary<AddonInfo, AddonBase>();
            Debug.engine_log("Initializing CSharpCodeProvider");
            m_cSharpCodeProvider = new CSharpCodeProvider();
            Debug.engine_log("Initializing CSharpCodeProvider done");
            LoadPlugins();
            LoadAll();
        }

        private void LoadPlugins() {
            Debug.engine_log("Loading Plugins");
            if (m_appDomain != null) {
                Debug.engine_log("Plugins are already loaded.");
                return;
            }

            m_appDomain = AppDomain.CurrentDomain;
            m_appDomain.AssemblyLoad += (sender, args) => {
                Debug.engine_log("[Plugins] Loading assembly " + args.LoadedAssembly.FullName);
            };
            m_appDomain.AssemblyResolve += (sender, args) => {
                Debug.engine_log("[Plugins] Resolving assembly " + args.Name);
                string libraryname = args.Name.Split(' ')[0];

                if (libraryname.Contains("SioBoxAPI")) {
                    return Assembly.GetExecutingAssembly();
                }

                foreach (var listAvailablePlugin in ListAvailablePlugins()) {
                    if(listAvailablePlugin.Contains(libraryname.Substring(0, libraryname.Length - 1)))
                        return Assembly.LoadFile(listAvailablePlugin);
                }

                return Assembly.Load(args.Name);
            };

            foreach (var path in ListAvailablePlugins()) {
                var assemblyName = AssemblyName.GetAssemblyName(Paths.FixPath(path));
                m_appDomain.Load(assemblyName);
            }
            
            Debug.engine_log("[Plugins] Successfully loaded.");
        }

        private void UnloadPlugins() {
            //AppDomain.Unload(m_appDomain);
            m_appDomain = null;
        }

        private void dependencyResolve(AddonInfo node, List<AddonInfo> resolved, List<AddonInfo> unresolved) {
            unresolved.Add(node);
            foreach (var dependency in node.Dependencies) {
                
            }
        }

        private void LoadAll() {
            Debug.engine_log("[AddonManager] Loading all addons.");
            List<AddonInfo> addonsInfos = new List<AddonInfo>();
            string[] paths = ListAddons();
            foreach (string path in paths) {
                Debug.engine_log("[AddonManager] Found addon at " + path + ": ");
                var validator = AddonInfo.GetFromFile(Paths.FixPath(path));
                if (validator) {
                    var nfo = validator.Value;
                    Debug.engine_log("[AddonManager] AddonInfo: " + nfo.ToString());
                    addonsInfos.Add(nfo);
                } else {
                    Debug.engine_error(validator.Error.Message);
                }
            }

            //addonsInfos.Sort((left, right) => {
            //    return 0;
            //});
            addonsInfos.ForEach(info => {
                var validator = Load(info);
                if (!validator) {
                    Debug.engine_error(validator.Error.Message);
                }
            });
        }

        private void UnloadAll() {
            
        }
        

        /// <summary>
        /// Return paths to all available addons in addons directory
        /// </summary>
        /// <returns></returns>
        public static string[] ListAddons() {
            try {
                return Directory.GetDirectories(Paths.AddonsDirectory);
            } catch (Exception e) {
                Debug.engine_error(e);
            }
            return new String[0];
        }

        public static string[] ListAvailablePlugins() {
            List<string> result = new List<string>();
            foreach (var path in ListPlugins())
            {
                if (path.Contains("SioBoxAPI")) continue;
                if ((Engine.IsX64 && path.EndsWith("x64")) || (!Engine.IsX64 && path.EndsWith("x86")))
                {
                    foreach (var pathToDll in Directory.GetFiles(path, "*.dll", SearchOption.AllDirectories))
                    {
                        if (pathToDll.Contains("SioBoxAPI")) continue;
                        result.Add(pathToDll);
                    }
                }
                else
                {
                    foreach (var pathToDll in Directory.GetFiles(path, "*.dll", SearchOption.AllDirectories))
                    {
                        if (pathToDll.Contains("SioBoxAPI")) continue;
                        result.Add(pathToDll);
                    }
                }
            }
            return result.ToArray();
        }

        public static string[] ListPlugins() {
            try
            {
                return Directory.GetDirectories(Paths.PluginsDirectory);
            }
            catch (Exception e)
            {
                Debug.engine_error(e);
            }
            return new String[0];
        }
        
        public static ErrorContainer<AddonBase> Load(AddonInfo addonInfo)
        {
            Debug.engine_log("[AddonManager] Loading addon " + addonInfo.ToString());
            Debug.engine_log("Creating array instance where m_instance = " + (m_instance) + ", m_addons = " + m_instance?.m_addons);

            AddonInfo[] addons = new AddonInfo[m_instance.m_addons.Count];
            Debug.engine_log("Found " + (m_instance?.m_addons?.Count) + " addons");
            m_instance.m_addons.Keys.CopyTo(addons, 0);
            Debug.engine_log("Getting dependencies");
            addonInfo.GetDependencies(addons);
            if (addonInfo.UnresolvedDependencies.Count > 0) return new Error(message: $"Missing dependencies [{String.Join(", ", addonInfo.UnresolvedDependencies.ToArray())}] for addon '{addonInfo.Id}'.").ToErrorContainer<AddonBase>();
            else Debug.engine_log("All dependencies found");


            Debug.engine_log("Checking if main script exists");
            if (!File.Exists(addonInfo.AddonMainScriptPath)) {
                return new Error(message: $"Missing mainscript file at path [{addonInfo.AddonMainScriptPath}]").ToErrorContainer<AddonBase>();
            }

            Debug.engine_log("Compiling...");
            
            var options = new CompilerParameters();
            options.ReferencedAssemblies.Add(Assembly.GetExecutingAssembly().Location);
            options.GenerateInMemory = false;
            options.IncludeDebugInformation = true;
            var outputDir = Paths.Combine(addonInfo.Path, "Compiled");
            var filePath = Paths.Combine(outputDir, Path.GetFileName(addonInfo.Path) + ".dll");
            if (!Directory.Exists(outputDir)) Directory.CreateDirectory(outputDir);
            if(File.Exists(filePath)) File.Delete(filePath);
            options.OutputAssembly = filePath;
            foreach (var listAvailablePlugin in ListAvailablePlugins()) {
                options.ReferencedAssemblies.Add(listAvailablePlugin);
            }

            var compilerResults = m_instance.m_cSharpCodeProvider.CompileAssemblyFromSource(options, File.ReadAllText(addonInfo.AddonMainScriptPath));

            Debug.engine_log("Compiled");
            if (compilerResults.Errors.HasErrors) {

                Debug.engine_log("Found errors");
                var logs = Paths.Combine(addonInfo.Path, "Logs");
                if (!Directory.Exists(logs))
                    Directory.CreateDirectory(logs);
                var logPath = Paths.Combine(logs, "error.log");
                List<string> lines = new List<string>();
                foreach (CompilerError compilerResultsError in compilerResults.Errors) {
                    var pathToFile = Paths.GetRelativePath(addonInfo.Path, compilerResultsError.FileName);
                    lines.Add($"[ERROR] File: {pathToFile}:{compilerResultsError.Line} [{compilerResultsError.ErrorNumber}] {compilerResultsError.ErrorText}");
                }
                File.WriteAllLines(logPath, lines.ToArray());
                return new Error(message: $"Compiler found errors in source-code. Writing to [{logPath}]").ToErrorContainer<AddonBase>();
            } else {

                Debug.engine_log("Addon was successfully compiled.");
            }
           
            Debug.engine_log("Trying to find AddonBase");
            AddonBase addon = null;
            var assembly = compilerResults.CompiledAssembly;
            try {
                foreach (var type in assembly.GetTypes()) {
                    Debug.engine_log("IT: " + type.FullName);
                    if(typeof(AddonBase).IsAssignableFrom(type)) {
                        addon = Activator.CreateInstance(type) as AddonBase;
                        break;
                    }
                }
            } catch (ReflectionTypeLoadException e) {
                foreach (var eLoaderException in e.LoaderExceptions) {
                    Debug.engine_error(eLoaderException);
                }
            }

            if (addon == null) {
                return new Error(message: $"Could not find class what extends AddonBase.").ToErrorContainer<AddonBase>();
            }

            Debug.engine_log("AddonBase found");

            m_instance.m_addons.Add(addonInfo, addon);
            addonInfo.State.SetLoaded();
            addon.init(addonInfo);
            return addon.ToErrorContainer();
        }

        public static bool Unload(AddonInfo addonInfo)
        {
            return true;
        }

        public static bool Enable(AddonInfo addonInfo)
        {
            return true;
        }

        public static bool Disable(AddonInfo addonInfo)
        {
            return true;
        }

        public static bool TryGet<TAddon>(string addonIdentification, ref TAddon addon) where TAddon : AddonBase
        {
            //if (!IsInstalled(addonIdentification)) return false;
            try {
                addon = m_instance.m_addons.First(pair => pair.Key.Id == addonIdentification).Value as TAddon;
            } catch (Exception e) {
                return false;
            }
            return true;
        }
    }
}