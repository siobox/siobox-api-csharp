﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SioBoxAPI.Exceptions
{
    public class EngineException : Exception
    {
        public EngineException(string message) : base(message) {
        }
    }
}
