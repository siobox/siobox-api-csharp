﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SioBoxAPI
{
    public struct Error
    {
        public Error(bool isValid = false, string message = null)
        {
            IsValid = isValid;
            Message = message;
        }

        public bool IsValid;
        public string Message;

        public void PrintToScreen()
        {
            Debug.LogOnScreen(Message, Color.Red);
        }
    }

    public static class ErrorExtension
    {
        public static ErrorContainer<Item> ToErrorContainer<Item>(this Item i, Error error = default(Error))
        {
            return ErrorContainer<Item>.From(i);
        }

        public static ErrorContainer<Item> ToErrorContainer<Item>(this Error error)
        {
            return ErrorContainer<Item>.FromError<Item>(error);
        }
    }

    public struct ErrorContainer<Item>
    {
        private Error error;
        private Item value;
        private bool isDefault;

        public ErrorContainer(Item value, Error error)
        {
            this.error = error;
            this.value = value;
            isDefault = false;
        }

        public Item Value => value;
        public Error Error => error;

        public static ErrorContainer<Item> FromError<Item>(Error error)
        {
            return new ErrorContainer<Item>(default(Item), error) { isDefault = true };
        }

        public static ErrorContainer<Item> From(Item item)
        {
            return new ErrorContainer<Item>(item, default(Error));
        }

        public static implicit operator bool(ErrorContainer<Item> item)
        {
            return !item.isDefault && !item.error.IsValid;
        }
    }
}
