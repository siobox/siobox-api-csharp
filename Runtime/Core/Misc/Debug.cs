﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace SioBoxAPI
{
    public class Debug
    {
        public static void LogOnScreen(string text, Color color = default(Color))
        {

        }

        [MethodImpl(MethodImplOptions.NoInlining)]
        public static void Log(string text) {
            StackTrace stackTrace = new StackTrace();
            var declaringType = stackTrace.GetFrame(1).GetMethod().DeclaringType;
            if (declaringType != null) engine_log($"[{Path.GetFileNameWithoutExtension(declaringType.Assembly.Location)}] {text}");
            else engine_log(text);
        }

        public static void Error(string text) {
            engine_error(text);
        }

        //[DllImport(G.DLL, EntryPoint = G.Runtime.Engine.TimerManager.StartTimer)]
        //internal static extern void _PRINT_STRING(out IntPtr InOutHandle, float InRate, bool InbLoop, float InFirstDelay);


        [DllImport(G.DLL, EntryPoint = G.Runtime.Core.Misc.Debug.EngineError, CallingConvention = CallingConvention.Cdecl)]
        internal static extern void _ENGINE_ERROR(string error);

        [DllImport(G.DLL, EntryPoint = G.Runtime.Core.Misc.Debug.EngineLog, CallingConvention = CallingConvention.Cdecl)]
        internal static extern void _ENGINE_LOG(string error);

        internal static void engine_error(Exception exception) {
            _ENGINE_ERROR(exception.Message + " " + exception.StackTrace);
        }

        internal static void engine_log(string message) {
            _ENGINE_LOG(message);
        }

        internal static void engine_error(string message)
        {
            _ENGINE_ERROR(message);
        }
    }
}
