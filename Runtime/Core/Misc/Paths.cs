﻿
using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;

namespace SioBoxAPI.Runtime.Core.Misc
{
    public class Paths
    {
        /// <summary>
        /// Returns the game directory
        /// </summary>
        public static string GameDirectory
        {
            get {
                int defaultStringSize = 256;
                StringBuilder sb = new StringBuilder(defaultStringSize);
                _GET_GAME_DIRECTORY(sb, ref defaultStringSize);
                return sb.ToString();
            }
        }

        /// <summary>
        /// Returns the directory where are logs
        /// </summary>
        public static string LogDirectory
        {
            get
            {
                int defaultStringSize = 256;
                StringBuilder sb = new StringBuilder(defaultStringSize);
                _GET_LOG_DIRECTORY(sb, ref defaultStringSize);
                return sb.ToString();
            }
        }

        /// <summary>
        /// Returns the directory where are addons
        /// </summary>
        public static string AddonsDirectory
        {
            get
            {
                int defaultStringSize = 256;
                StringBuilder sb = new StringBuilder(defaultStringSize);
                _GET_ADDONS_DIRECTORY(sb, ref defaultStringSize);
                return sb.ToString();
            }
        }

        /// <summary>
        /// Returns the directory where are stored plugins
        /// </summary>
        public static string PluginsDirectory
        {
            get
            {
                int defaultStringSize = 256;
                StringBuilder sb = new StringBuilder(defaultStringSize);
                _GET_PLUGINS_DIRECTORY(sb, ref defaultStringSize);
                return sb.ToString();
            }
        }

        /// <summary>
        /// Returns the directory where are stored saves
        /// </summary>
        public static string SavesDirectory
        {
            get
            {
                int defaultStringSize = 256;
                StringBuilder sb = new StringBuilder(defaultStringSize);
                _GET_SAVES_DIRECTORY(sb, ref defaultStringSize);
                return sb.ToString();
            }
        }

        /// <summary>
        /// Returns the directory the application was launched from(useful for commandline utilities)
        /// </summary>
        public static string LaunchDirectory
        {
            get
            {
                int defaultStringSize = 256;
                StringBuilder sb = new StringBuilder(defaultStringSize);
                _GET_LAUNCH_DIRECTORY(sb, ref defaultStringSize);
                return sb.ToString();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static string Combine(string a, string b) {
            int defaultStringSize = a.Length + b.Length + 10;
            StringBuilder sb = new StringBuilder(defaultStringSize);
            _COMBINE(a, b, sb, ref defaultStringSize);
            return sb.ToString();
        }

        public static string FixPath(string path) {
            return path.Replace('/', Path.DirectorySeparatorChar).Replace('\\', Path.DirectorySeparatorChar);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <example>
        /// var path GetRelativePath("c:/dev/foo/bar", "c:/dev/foo/bar/docs/readme.txt")
        /// path == "docs/readme.txt"
        /// </example>
        /// <param name="workingDirectory"></param>
        /// <param name="fullPath"></param>
        /// <returns></returns>
        public static string GetRelativePath(string workingDirectory, string fullPath)
        {
            string result = string.Empty;
            int offset;

            // this is the easy case.  The file is inside of the working directory.
            if (fullPath.StartsWith(workingDirectory))
            {
                return fullPath.Substring(workingDirectory.Length + 1);
            }

            // the hard case has to back out of the working directory
            string[] baseDirs = workingDirectory.Split(new char[] { ':', '\\', '/' });
            string[] fileDirs = fullPath.Split(new char[] { ':', '\\', '/' });

            // if we failed to split (empty strings?) or the drive letter does not match
            if (baseDirs.Length <= 0 || fileDirs.Length <= 0 || baseDirs[0] != fileDirs[0])
            {
                // can't create a relative path between separate harddrives/partitions.
                return fullPath;
            }

            // skip all leading directories that match
            for (offset = 1; offset < baseDirs.Length; offset++)
            {
                if (baseDirs[offset] != fileDirs[offset])
                    break;
            }

            // back out of the working directory
            for (int i = 0; i < (baseDirs.Length - offset); i++)
            {
                result += "..\\";
            }

            // step into the file path
            for (int i = offset; i < fileDirs.Length - 1; i++)
            {
                result += fileDirs[i] + "\\";
            }

            // append the file
            result += fileDirs[fileDirs.Length - 1];

            return result;
        }

        [DllImport(G.DLL, EntryPoint = G.Runtime.Core.Misc.Paths.GetGameDirectory, CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        internal static extern void _GET_GAME_DIRECTORY(StringBuilder InGameDir, ref int InOutCapacity);

        [DllImport(G.DLL, EntryPoint = G.Runtime.Core.Misc.Paths.GetLogDirectory, CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        internal static extern void _GET_LOG_DIRECTORY(StringBuilder InLogDir, ref int InOutCapacity);

        [DllImport(G.DLL, EntryPoint = G.Runtime.Core.Misc.Paths.GetAddonsDirectory, CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        internal static extern void _GET_ADDONS_DIRECTORY(StringBuilder InAddonsDir, ref int InOutCapacity);

        [DllImport(G.DLL, EntryPoint = G.Runtime.Core.Misc.Paths.GetPluginsDirectory, CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        internal static extern void _GET_PLUGINS_DIRECTORY(StringBuilder InPluginsDir, ref int InOutCapacity);

        [DllImport(G.DLL, EntryPoint = G.Runtime.Core.Misc.Paths.GetSavesDirectory, CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        internal static extern void _GET_SAVES_DIRECTORY(StringBuilder InSavesDir, ref int InOutCapacity);

        [DllImport(G.DLL, EntryPoint = G.Runtime.Core.Misc.Paths.GetLaunchDirectory, CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        internal static extern void _GET_LAUNCH_DIRECTORY(StringBuilder InLaunchDir, ref int InOutCapacity);

        [DllImport(G.DLL, EntryPoint = G.Runtime.Core.Misc.Paths.GetCombine, CallingConvention = CallingConvention.Cdecl, CharSet = CharSet.Ansi)]
        internal static extern void _COMBINE(string InPathA, string InPathB, StringBuilder InCombinedPath, ref int InOutCapacity);
    }
}
