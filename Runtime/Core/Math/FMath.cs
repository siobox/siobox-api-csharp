﻿namespace SioBoxAPI.Runtime.Core.Math
{
    public class FMath
    {
        public static float Lerp(float a, float b, float time)
        {
            return a + time * (b - a);
        }

        public static uint Lerp(uint a, uint b, float time)
        {
            return (uint)(a + time * (b - a));
        }
    }
}
