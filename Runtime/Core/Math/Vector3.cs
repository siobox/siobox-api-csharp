﻿using System;

namespace SioBoxAPI.Runtime.Core.Math
{
    public struct Vector3
    {
        private float x, y, z;

        public Vector3(float x, float y, float z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        /// <summary>
        /// Add a vector to this and clamp the result in a cube.
        /// </summary>
        /// <param name="V">Vector</param>
        /// <param name="Radius">Radius</param>
        public void AddBounded(Vector3 V, float Radius)
        {

        }

        /// <summary>
        /// Get a copy of this vector, clamped inside of a cube. A copy of this vector, bound by cube.
        /// </summary>
        /// <param name="Radius"></param>
        /// <returns></returns>
        public Vector3 BoundToCube(float Radius)
        {
            return default(Vector3);
        }

        /// <summary>
        /// Compute pushout of a box from a plane. Pushout required.
        /// </summary>
        /// <param name="Normal"></param>
        /// <param name="Size"></param>
        /// <returns></returns>
        public static float BoxPushOut(Vector3 Normal, Vector3 Size)
        {
            return 0;
        }

        /// <summary>
        /// See if two normal vectors are coincident (nearly parallel and point in the same direction).
        /// </summary>
        /// <param name="Normal1"></param>
        /// <param name="Normal2"></param>
        /// <param name="ParallelCosineThreshold"></param>
        /// <returns></returns>
        public static bool Coincident(Vector3 Normal1, Vector3 Normal2, float ParallelCosineThreshold)
        {
            return false;
        }

        /// <summary>
        /// Gets a specific component of the vector. Copy of the specified component.
        /// </summary>
        /// <param name="Index"></param>
        /// <returns></returns>
        public float Component(Int32 Index)
        {
            return 0;
        }

        /// <summary>
        /// Gets the component-wise max of two vectors.
        /// </summary>
        /// <param name="Other"></param>
        /// <returns></returns>
        public Vector3 ComponentMax(Vector3 Other)
        {
            return default(Vector3);
        }

        /// <summary>
        /// Gets the component-wise min of two vectors.
        /// </summary>
        /// <param name="Other"></param>
        /// <returns></returns>
        public Vector3 ComponentMin(Vector3 Other)
        {
            return default(Vector3);
        }

        /// <summary>
        /// Utility to check if there are any non-finite values (NaN or Inf) in this vector.
        /// </summary>
        /// <returns></returns>
        public bool ContainsNaN()
        {
            return false;
        }

        /// <summary>
        /// See if two planes are coplanar.
        /// </summary>
        /// <param name="Base1"></param>
        /// <param name="Normal1"></param>
        /// <param name="Base2"></param>
        /// <param name="Normal2"></param>
        /// <param name="ParallelCosineThreshold"></param>
        /// <returns></returns>
        public static bool Coplanar(Vector3 Base1, Vector3 Normal1, Vector3 Base2, Vector3 Normal2, float ParallelCosineThreshold) 
        {
            return false;
        }

        /// <summary>
        /// Returns the cosine of the angle between this vector and another projected onto the XY plane (no Z).
        /// </summary>
        /// <param name="B"></param>
        /// <returns></returns>
        public float CosineAngle2D(Vector3 B)
        {
            return 0f;
        }

        [Obsolete]
        public static void CreateOrthonormalBasis(Vector3 XAxis, Vector3 YAxis, Vector3 ZAxis)
        {

        }

        /// <summary>
        /// Calculate the cross product of two vectors. The cross product.
        /// </summary>
        /// <param name="A"></param>
        /// <param name="B"></param>
        /// <returns></returns>
        public static Vector3 CrossProduct(Vector3 A, Vector3 B)
        {
            return default(Vector3);
        }

        /// <summary>
        /// Converts a vector containing degree values to a vector containing radian values.
        /// </summary>
        /// <param name="DegVector"></param>
        /// <returns></returns>
        public static Vector3 DegressToRadians(Vector3 DegVector)
        {
            return default(Vector3);
        }

        /// <summary>
        /// Euclidean distance between two points. The distance between two points.
        /// </summary>
        /// <param name="V1"></param>
        /// <param name="V2"></param>
        /// <returns></returns>
        public static float Dist(Vector3 V1, Vector3 V2)
        {
            return 0f;
        }

        /// <summary>
        /// Euclidean distance between two points. The distance between two points.
        /// </summary>
        /// <param name="V1"></param>
        /// <param name="V2"></param>
        /// <returns></returns>
        public static float Dist2D(Vector3 V1, Vector3 V2)
        {
            return 0f;
        }

        public static float Distance(Vector3 V1, Vector3 V2)
        {
            return 0f;
        }

        /// <summary>
        /// Squared distance between two points. The squared distance between two points.
        /// </summary>
        /// <param name="V1"></param>
        /// <param name="V2"></param>
        /// <returns></returns>
        public static float DistSquared(Vector3 V1, Vector3 V2)
        {
            return 0f;
        }

        /// <summary>
        /// Squared distance between two points. The squared distance between two points.
        /// </summary>
        /// <param name="V1"></param>
        /// <param name="V2"></param>
        /// <returns></returns>
        public static float DistSquared2D(Vector3 V1, Vector3 V2)
        {
            return 0f;
        }

        /// <summary>
        /// Squared distance between two points in the XY plane only.
        /// </summary>
        /// <param name="V1"></param>
        /// <param name="V2"></param>
        /// <returns></returns>
        public static float DistSquaredXY(Vector3 V1, Vector3 V2)
        {
            return 0f;
        }

        /// <summary>
        /// Euclidean distance between two points in the XY plane (ignoring Z).
        /// </summary>
        /// <param name="V1"></param>
        /// <param name="V2"></param>
        /// <returns></returns>
        public static float DistXY(Vector3 V1, Vector3 V2)
        {
            return 0f;
        }

        /// <summary>
        /// Calculate the dot product of two vectors. The dot product.
        /// </summary>
        /// <param name="V1"></param>
        /// <param name="V2"></param>
        /// <returns></returns>
        public static float DotProduct(Vector3 V1, Vector3 V2)
        {
            return 0f;
        }


    }
}
