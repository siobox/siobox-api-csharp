﻿using System;
using System.Runtime.InteropServices;
using System.Text;
using SioBoxAPI.Runtime.Core;
using SioBoxAPI.Runtime.Core.Math;

namespace SioBoxAPI
{
    [Serializable]
    public struct Color : ICloneable<Color>
    {
        private uint r, g, b, a;
        private bool isDefault;

        public Color(uint r, uint g, uint b, uint a = 1)
        {
            this.r = r;
            this.g = g;
            this.b = b;
            this.a = a;
            this.isDefault = false;
        }
        
        internal IntPtr FColor
        {
            get
            {
                IntPtr ptr;
                _GET_FCOLOR(out ptr, r, g, b, a);
                return ptr;
            }
        }

        public string Hex
        {
            get
            {
                StringBuilder builder = new StringBuilder(capacity: 7 * 8);
                builder.Append("#").Append(r.ToString("X")).Append(g.ToString("X")).Append(b.ToString("X"));
                return builder.ToString();
            }
        }

        public string HexWithAlpha
        {
            get
            {
                StringBuilder builder = new StringBuilder(capacity: 9 * 8);
                builder.Append("#").Append(r.ToString("X")).Append(g.ToString("X")).Append(b.ToString("X")).Append(a.ToString("X"));
                return builder.ToString();
            }
        }

        public static Color FromHex(string hex)
        {
            if (hex.StartsWith("#")) hex = hex.Substring(1);
            if(hex.Length == 6)
            {
                string r = hex.Substring(0, 2);
                string g = hex.Substring(2, 2);
                string b = hex.Substring(4, 2);
                return new Color(uint.Parse(r, System.Globalization.NumberStyles.HexNumber), uint.Parse(g, System.Globalization.NumberStyles.HexNumber), uint.Parse(b, System.Globalization.NumberStyles.HexNumber));
            } else if (hex.Length == 8)
            {
                string r = hex.Substring(0, 2);
                string g = hex.Substring(2, 2);
                string b = hex.Substring(4, 2);
                string a = hex.Substring(6, 2);
                return new Color(uint.Parse(r, System.Globalization.NumberStyles.HexNumber), uint.Parse(g, System.Globalization.NumberStyles.HexNumber), uint.Parse(b, System.Globalization.NumberStyles.HexNumber), uint.Parse(a, System.Globalization.NumberStyles.HexNumber));

            }
            throw new Exception("Invalid hex (req. length 6 or 8, # is removed if is there), given length is " + hex.Length + ". Example: #f1f1f1 or f1f1f1 or #ff121212 or ff121212");
        }

        public uint A
        {
            get
            {
                return a;
            }
            set
            {
                if (!isDefault)
                    a = value;
                else throw new Exception("You're trying to modify default color. Maybe you forget to clone it? E.g. var myColor = Color.Red.Cloned; myColor.A = 0;");
            }
        }


        public uint R
        {
            get
            {
                return r;
            }
            set
            {
                if(!isDefault)
                    r = value;
                else throw new Exception("You're trying to modify default color. Maybe you forget to clone it? E.g. var myColor = Color.Red.Cloned; myColor.R = 0;");
            }
        }


        public uint G
        {
            get
            {
                return g;
            }
            set
            {
                if (!isDefault)
                    g = value;
                else throw new Exception("You're trying to modify default color. Maybe you forget to clone it? E.g. var myColor = Color.Red.Cloned; myColor.G = 0;");
            }
        }


        public uint B
        {
            get
            {
                return b;
            }
            set
            {
                if (!isDefault)
                    b = value;
                else throw new Exception("You're trying to modify default color. Maybe you forget to clone it? E.g. var myColor = Color.Red.Cloned; myColor.B = 0;");
            }
        }

        public static Color Lerp(Color from, Color to, float time)
        {
            return new Color(FMath.Lerp(from.R, to.R, time), FMath.Lerp(from.G, to.G, time), FMath.Lerp(from.B, to.B, time), FMath.Lerp(from.A, to.A, time));
        }

        public static Color LerpAlpha(Color color, uint alpha, float time)
        {
            return new Color(color.R, color.G, color.B, FMath.Lerp(color.A, alpha, time));
        }

        public static readonly Color Aliceblue = new Color(240, 248, 255) { isDefault = true };
        public static readonly Color Antiquewhite = new Color(250, 235, 215) { isDefault = true };
        public static readonly Color Aqua = new Color(0, 255, 255) { isDefault = true };
        public static readonly Color Aquamarine = new Color(127, 255, 212) { isDefault = true };
        public static readonly Color Azure = new Color(240, 255, 255) { isDefault = true };
        public static readonly Color Beige = new Color(245, 245, 220) { isDefault = true };
        public static readonly Color Bisque = new Color(255, 228, 196) { isDefault = true };
        public static readonly Color Black = new Color(0, 0, 0) { isDefault = true };
        public static readonly Color Blanchedalmond = new Color(255, 235, 205) { isDefault = true };
        public static readonly Color Blue = new Color(0, 0, 255) { isDefault = true };
        public static readonly Color Blueviolet = new Color(138, 43, 226) { isDefault = true };
        public static readonly Color Brown = new Color(165, 42, 42) { isDefault = true };
        public static readonly Color Burlywood = new Color(222, 184, 135) { isDefault = true };
        public static readonly Color Cadetblue = new Color(95, 158, 160) { isDefault = true };
        public static readonly Color Chartreuse = new Color(127, 255, 0) { isDefault = true };
        public static readonly Color Chocolate = new Color(210, 105, 30) { isDefault = true };
        public static readonly Color Coral = new Color(255, 127, 80) { isDefault = true };
        public static readonly Color Cornflowerblue = new Color(100, 149, 237) { isDefault = true };
        public static readonly Color Cornsilk = new Color(255, 248, 220) { isDefault = true };
        public static readonly Color Crimson = new Color(220, 20, 60) { isDefault = true };
        public static readonly Color Cyan = new Color(0, 255, 255) { isDefault = true };
        public static readonly Color Darkblue = new Color(0, 0, 139) { isDefault = true };
        public static readonly Color Darkcyan = new Color(0, 139, 139) { isDefault = true };
        public static readonly Color Darkgoldenrod = new Color(184, 134, 11) { isDefault = true };
        public static readonly Color Darkgray = new Color(169, 169, 169) { isDefault = true };
        public static readonly Color Darkgreen = new Color(0, 100, 0) { isDefault = true };
        public static readonly Color Darkgrey = new Color(169, 169, 169) { isDefault = true };
        public static readonly Color Darkkhaki = new Color(189, 183, 107) { isDefault = true };
        public static readonly Color Darkmagenta = new Color(139, 0, 139) { isDefault = true };
        public static readonly Color Darkolivegreen = new Color(85, 107, 47) { isDefault = true };
        public static readonly Color Darkorange = new Color(255, 140, 0) { isDefault = true };
        public static readonly Color Darkorchid = new Color(153, 50, 204) { isDefault = true };
        public static readonly Color Darkred = new Color(139, 0, 0) { isDefault = true };
        public static readonly Color Darksalmon = new Color(233, 150, 122) { isDefault = true };
        public static readonly Color Darkseagreen = new Color(143, 188, 143) { isDefault = true };
        public static readonly Color Darkslateblue = new Color(72, 61, 139) { isDefault = true };
        public static readonly Color Darkslategray = new Color(47, 79, 79) { isDefault = true };
        public static readonly Color Darkslategrey = new Color(47, 79, 79) { isDefault = true };
        public static readonly Color Darkturquoise = new Color(0, 206, 209) { isDefault = true };
        public static readonly Color Darkviolet = new Color(148, 0, 211) { isDefault = true };
        public static readonly Color Deeppink = new Color(255, 20, 147) { isDefault = true };
        public static readonly Color Deepskyblue = new Color(0, 191, 255) { isDefault = true };
        public static readonly Color Dimgray = new Color(105, 105, 105) { isDefault = true };
        public static readonly Color Dimgrey = new Color(105, 105, 105) { isDefault = true };
        public static readonly Color Dodgerblue = new Color(30, 144, 255) { isDefault = true };
        public static readonly Color Firebrick = new Color(178, 34, 34) { isDefault = true };
        public static readonly Color Floralwhite = new Color(255, 250, 240) { isDefault = true };
        public static readonly Color Forestgreen = new Color(34, 139, 34) { isDefault = true };
        public static readonly Color Fuchsia = new Color(255, 0, 255) { isDefault = true };
        public static readonly Color Gainsboro = new Color(220, 220, 220) { isDefault = true };
        public static readonly Color Ghostwhite = new Color(248, 248, 255) { isDefault = true };
        public static readonly Color Gold = new Color(255, 215, 0) { isDefault = true };
        public static readonly Color Goldenrod = new Color(218, 165, 32) { isDefault = true };
        public static readonly Color Gray = new Color(128, 128, 128) { isDefault = true };
        public static readonly Color Green = new Color(0, 128, 0) { isDefault = true };
        public static readonly Color Greenyellow = new Color(173, 255, 47) { isDefault = true };
        public static readonly Color Grey = new Color(128, 128, 128) { isDefault = true };
        public static readonly Color Honeydew = new Color(240, 255, 240) { isDefault = true };
        public static readonly Color Hotpink = new Color(255, 105, 180) { isDefault = true };
        public static readonly Color Indianred = new Color(205, 92, 92) { isDefault = true };
        public static readonly Color Indigo = new Color(75, 0, 130) { isDefault = true };
        public static readonly Color Ivory = new Color(255, 255, 240) { isDefault = true };
        public static readonly Color Khaki = new Color(240, 230, 140) { isDefault = true };
        public static readonly Color Lavender = new Color(230, 230, 250) { isDefault = true };
        public static readonly Color Lavenderblush = new Color(255, 240, 245) { isDefault = true };
        public static readonly Color Lawngreen = new Color(124, 252, 0) { isDefault = true };
        public static readonly Color Lemonchiffon = new Color(255, 250, 205) { isDefault = true };
        public static readonly Color Lightblue = new Color(173, 216, 230) { isDefault = true };
        public static readonly Color Lightcoral = new Color(240, 128, 128) { isDefault = true };
        public static readonly Color Lightcyan = new Color(224, 255, 255) { isDefault = true };
        public static readonly Color Lightgoldenrodyellow = new Color(250, 250, 210) { isDefault = true };
        public static readonly Color Lightgray = new Color(211, 211, 211) { isDefault = true };
        public static readonly Color Lightgreen = new Color(144, 238, 144) { isDefault = true };
        public static readonly Color Lightgrey = new Color(211, 211, 211) { isDefault = true };
        public static readonly Color Lightpink = new Color(255, 182, 193) { isDefault = true };
        public static readonly Color Lightsalmon = new Color(255, 160, 122) { isDefault = true };
        public static readonly Color Lightseagreen = new Color(32, 178, 170) { isDefault = true };
        public static readonly Color Lightskyblue = new Color(135, 206, 250) { isDefault = true };
        public static readonly Color Lightslategray = new Color(119, 136, 153) { isDefault = true };
        public static readonly Color Lightslategrey = new Color(119, 136, 153) { isDefault = true };
        public static readonly Color Lightsteelblue = new Color(176, 196, 222) { isDefault = true };
        public static readonly Color Lightyellow = new Color(255, 255, 224) { isDefault = true };
        public static readonly Color Lime = new Color(0, 255, 0) { isDefault = true };
        public static readonly Color Limegreen = new Color(50, 205, 50) { isDefault = true };
        public static readonly Color Linen = new Color(250, 240, 230) { isDefault = true };
        public static readonly Color Magenta = new Color(255, 0, 255) { isDefault = true };
        public static readonly Color Maroon = new Color(128, 0, 0) { isDefault = true };
        public static readonly Color Mediumaquamarine = new Color(102, 205, 170) { isDefault = true };
        public static readonly Color Mediumblue = new Color(0, 0, 205) { isDefault = true };
        public static readonly Color Mediumorchid = new Color(186, 85, 211) { isDefault = true };
        public static readonly Color Mediumpurple = new Color(147, 112, 219) { isDefault = true };
        public static readonly Color Mediumseagreen = new Color(60, 179, 113) { isDefault = true };
        public static readonly Color Mediumslateblue = new Color(123, 104, 238) { isDefault = true };
        public static readonly Color Mediumspringgreen = new Color(0, 250, 154) { isDefault = true };
        public static readonly Color Mediumturquoise = new Color(72, 209, 204) { isDefault = true };
        public static readonly Color Mediumvioletred = new Color(199, 21, 133) { isDefault = true };
        public static readonly Color Midnightblue = new Color(25, 25, 112) { isDefault = true };
        public static readonly Color Mintcream = new Color(245, 255, 250) { isDefault = true };
        public static readonly Color Mistyrose = new Color(255, 228, 225) { isDefault = true };
        public static readonly Color Moccasin = new Color(255, 228, 181) { isDefault = true };
        public static readonly Color Navajowhite = new Color(255, 222, 173) { isDefault = true };
        public static readonly Color Navy = new Color(0, 0, 128) { isDefault = true };
        public static readonly Color Oldlace = new Color(253, 245, 230) { isDefault = true };
        public static readonly Color Olive = new Color(128, 128, 0) { isDefault = true };
        public static readonly Color Olivedrab = new Color(107, 142, 35) { isDefault = true };
        public static readonly Color Orange = new Color(255, 165, 0) { isDefault = true };
        public static readonly Color Orangered = new Color(255, 69, 0) { isDefault = true };
        public static readonly Color Orchid = new Color(218, 112, 214) { isDefault = true };
        public static readonly Color Palegoldenrod = new Color(238, 232, 170) { isDefault = true };
        public static readonly Color Palegreen = new Color(152, 251, 152) { isDefault = true };
        public static readonly Color Paleturquoise = new Color(175, 238, 238) { isDefault = true };
        public static readonly Color Palevioletred = new Color(219, 112, 147) { isDefault = true };
        public static readonly Color Papayawhip = new Color(255, 239, 213) { isDefault = true };
        public static readonly Color Peachpuff = new Color(255, 218, 185) { isDefault = true };
        public static readonly Color Peru = new Color(205, 133, 63) { isDefault = true };
        public static readonly Color Pink = new Color(255, 192, 203) { isDefault = true };
        public static readonly Color Plum = new Color(221, 160, 221) { isDefault = true };
        public static readonly Color Powderblue = new Color(176, 224, 230) { isDefault = true };
        public static readonly Color Purple = new Color(128, 0, 128) { isDefault = true };
        public static readonly Color Rebeccapurple = new Color(102, 51, 153) { isDefault = true };
        public static readonly Color Red = new Color(255, 0, 0) { isDefault = true };
        public static readonly Color Rosybrown = new Color(188, 143, 143) { isDefault = true };
        public static readonly Color Royalblue = new Color(65, 105, 225) { isDefault = true };
        public static readonly Color Saddlebrown = new Color(139, 69, 19) { isDefault = true };
        public static readonly Color Salmon = new Color(250, 128, 114) { isDefault = true };
        public static readonly Color Sandybrown = new Color(244, 164, 96) { isDefault = true };
        public static readonly Color Seagreen = new Color(46, 139, 87) { isDefault = true };
        public static readonly Color Seashell = new Color(255, 245, 238) { isDefault = true };
        public static readonly Color Sienna = new Color(160, 82, 45) { isDefault = true };
        public static readonly Color Silver = new Color(192, 192, 192) { isDefault = true };
        public static readonly Color Skyblue = new Color(135, 206, 235) { isDefault = true };
        public static readonly Color Slateblue = new Color(106, 90, 205) { isDefault = true };
        public static readonly Color Slategray = new Color(112, 128, 144) { isDefault = true };
        public static readonly Color Slategrey = new Color(112, 128, 144) { isDefault = true };
        public static readonly Color Snow = new Color(255, 250, 250) { isDefault = true };
        public static readonly Color Springgreen = new Color(0, 255, 127) { isDefault = true };
        public static readonly Color Steelblue = new Color(70, 130, 180) { isDefault = true };
        public static readonly Color Tan = new Color(210, 180, 140) { isDefault = true };
        public static readonly Color Teal = new Color(0, 128, 128) { isDefault = true };
        public static readonly Color Thistle = new Color(216, 191, 216) { isDefault = true };
        public static readonly Color Tomato = new Color(255, 99, 71) { isDefault = true };
        public static readonly Color Turquoise = new Color(64, 224, 208) { isDefault = true };
        public static readonly Color Violet = new Color(238, 130, 238) { isDefault = true };
        public static readonly Color Wheat = new Color(245, 222, 179) { isDefault = true };
        public static readonly Color White = new Color(255, 255, 255) { isDefault = true };
        public static readonly Color Whitesmoke = new Color(245, 245, 245) { isDefault = true };
        public static readonly Color Yellow = new Color(255, 255, 0) { isDefault = true };
        public static readonly Color Yellowgreen = new Color(154, 205, 50) { isDefault = true };

        [DllImport(SioBoxAPI.G.DLL, EntryPoint = SioBoxAPI.G.Runtime.Core.Math.Color.GetFColor)]
        internal static extern void _GET_FCOLOR(out IntPtr FColor, uint r, uint g, uint b, uint a);

        public Color Cloned
        {
            get
            {
                return new Color(r, g, b, a);
            }
        }
    }

    
}
