﻿using System.Runtime.InteropServices;

namespace SioBoxAPI.Runtime.Core
{
    /// <summary>
    /// Supports cloning, which creates a new instance of a class with the same value as an existing instance.
    /// </summary>
    /// <typeparam name="Type"></typeparam>
    [ComVisible(true)]
    public interface ICloneable<Type>
    {
        /// <summary>
        /// Creates a new object that is a copy of the current instance.
        /// </summary>
        /// <returns>A new object that is a copy of this instance.</returns>
        Type Cloned { get; }
    }
}
