﻿namespace SioBoxAPI.Runtime.Engine
{
    public struct WorldInfo {
        public string Name;
        public string Path;
        public string Description;
    }
}
