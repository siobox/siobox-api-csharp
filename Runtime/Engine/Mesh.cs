﻿using SioBoxAPI.Runtime.Core.Math;

namespace SioBoxAPI.Runtime.Engine
{
    public class Mesh
    {

        public static readonly Mesh Box = new Mesh();
        public static readonly Mesh Sphere = new Mesh();
        public static readonly Mesh Plane = new Mesh();
    }
}
