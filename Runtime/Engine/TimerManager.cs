﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using static SioBoxAPI.Runtime.Engine.TimerManager;

namespace SioBoxAPI.Runtime.Engine
{

    public class TimerManager
    {
        private static Dictionary<string, TimerHandle> m_timers;

        internal static void initialize_engine() {
            m_timers = new Dictionary<string, TimerHandle>();
        }

        internal static void deinitialize_engine() {
            foreach (var keyValuePair in m_timers) {
                keyValuePair.Value.Stop();
            }
            m_timers.Clear();
            m_timers = null;
        }
    
        public static ErrorContainer<TimerHandle> StartTimer(string identification, TimerDelegate tickCallback, float rate, bool loop = false, float startDelay = 0f)
        {
            if (m_timers.ContainsKey(identification))
                return new Error(true, $"Key with given name {identification} already exists.").ToErrorContainer<TimerHandle>();

            _START_TIMER(out var handle, rate, loop, startDelay, Marshal.GetFunctionPointerForDelegate(tickCallback));
            var timerHandle = new TimerHandle(handle);
            m_timers.Add(identification, timerHandle);
            return timerHandle.ToErrorContainer();
        }

        [DllImport(G.DLL, EntryPoint = G.Runtime.Engine.TimerManager.StartTimer)]
        internal static extern void _START_TIMER(out IntPtr InOutHandle, float InRate, bool InbLoop, float InFirstDelay, IntPtr InCallback);
        
        [DllImport(G.DLL, EntryPoint = G.Runtime.Engine.TimerManager.StopTimer)]
        internal static extern void _STOP_TIMER(IntPtr InHandle);

        [DllImport(G.DLL, EntryPoint = G.Runtime.Engine.TimerManager.IsTimerPending)]
        internal static extern void _IS_TIMER_PENDING(IntPtr InHandle, out bool Pending);

        [DllImport(G.DLL, EntryPoint = G.Runtime.Engine.TimerManager.IsTimerExists)]
        internal static extern void _IS_TIMER_EXISTS(IntPtr InHandle, out bool Exists);

        [DllImport(G.DLL, EntryPoint = G.Runtime.Engine.TimerManager.IsTimerPaused)]
        internal static extern void _IS_TIMER_PAUSED(IntPtr InHandle, out bool Paused);

        [DllImport(G.DLL, EntryPoint = G.Runtime.Engine.TimerManager.IsTimerActive)]
        internal static extern void _IS_TIMER_ACTIVE(IntPtr InHandle, out bool Active);

        [DllImport(G.DLL, EntryPoint = G.Runtime.Engine.TimerManager.HasBeenTickedThisFrame)]
        internal static extern void _HAS_BEEN_TICKED_THIS_FRAME(out bool Ticked);

        [DllImport(G.DLL, EntryPoint = G.Runtime.Engine.TimerManager.UnpauseTimer)]
        internal static extern void _UNPAUSE_TIMER(IntPtr InHandle);

        [DllImport(G.DLL, EntryPoint = G.Runtime.Engine.TimerManager.PauseTimer)]
        internal static extern void _PAUSE_TIMER(IntPtr InHandle);

        [DllImport(G.DLL, EntryPoint = G.Runtime.Engine.TimerManager.GetTimerElapsed)]
        internal static extern void _GET_TIMER_ELAPSED(IntPtr InHandle, out float ElapsedTime);

        [DllImport(G.DLL, EntryPoint = G.Runtime.Engine.TimerManager.GetTimerRate)]
        internal static extern void _GET_TIMER_RATE(IntPtr InHandle, out float Rate);

        [DllImport(G.DLL, EntryPoint = G.Runtime.Engine.TimerManager.GetTimerRemaining)]
        internal static extern void _GET_TIMER_REMAINING(IntPtr InHandle, out float RemainingTime);
    }

    public delegate void TimerDelegate();

    public class TimerHandle
    {
        private readonly IntPtr m_handle;

        internal TimerHandle(IntPtr handle)
        {
            this.m_handle = handle;
        }
        
        public bool Paused {
            get {
                _IS_TIMER_PAUSED(m_handle, out var paused);
                return paused;
            }
            set {
                if(value)
                {
                    _PAUSE_TIMER(m_handle);
                } else
                {
                    _UNPAUSE_TIMER(m_handle);
                }
            }
        }

        public bool Exists
        {
            get
            {
                _IS_TIMER_EXISTS(m_handle, out var exists);
                return exists;
            }
        }

        public bool Active
        {
            get
            {
                _IS_TIMER_ACTIVE(m_handle, out var active);
                return active;
            }
        }

        public bool Pending
        {
            get
            {
                _IS_TIMER_PENDING(m_handle, out var pending);
                return pending;
            }
        }

        public static bool HasBeenTickedThisFrame
        {
            get
            {
                _HAS_BEEN_TICKED_THIS_FRAME(out var ticked);
                return ticked;
            }
        }

        public float RemainingTime
        {
            get
            {
                _GET_TIMER_REMAINING(m_handle, out var remaining);
                return remaining;
            }
        }

        public float RateTime
        {
            get
            {
                _GET_TIMER_RATE(m_handle, out var rate);
                return rate;
            }
        }

        public float ElapsedTime
        {
            get
            {
                _GET_TIMER_ELAPSED(m_handle, out var elapsed);
                return elapsed;
            }
        }

        public void Stop()
        {
            _STOP_TIMER(m_handle);
        }
    }
}
