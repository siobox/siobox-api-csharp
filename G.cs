﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization.Formatters;
using System.Text;

namespace SioBoxAPI
{
    internal static class G
    {
        public const string DLL = "D:\\PROJECTS\\Sionzee\\_UNREAL\\SioBox\\Binaries\\Win64\\UE4Editor-SioBox.dll";

        internal static class Runtime
        {
            internal static class Engine
            {
                internal static class TimerManager
                {
                    public const string 
                        StartTimer = "SIOBOX_TimerManager_StartTimer",
                        StopTimer = "SIOBOX_TimerManager_StopTimer",
                        IsTimerPending = "SIOBOX_TimerManager_IsTimerPending",
                        IsTimerExists = "SIOBOX_TimerManager_IsTimerExists",
                        IsTimerPaused = "SIOBOX_TimerManager_IsTimerPaused",
                        IsTimerActive = "SIOBOX_TimerManager_IsTimerActive",
                        HasBeenTickedThisFrame = "SIOBOX_TimerManager_HasBeenTickedThisFrame",
                        UnpauseTimer = "SIOBOX_TimerManager_UnpauseTimer",
                        PauseTimer = "SIOBOX_TimerManager_PauseTimer",
                        GetTimerElapsed = "SIOBOX_TimerManager_GetTimerElapsed",
                        GetTimerRate = "SIOBOX_TimerManager_GetTimerRate",
                        GetTimerRemaining = "SIOBOX_TimerManager_GetTimerRemaining";
                }
            }
            internal static class Core
            {
                internal static class Math
                {
                    internal static class Color
                    {
                        public const string
                            GetFColor = "GetFColor";
                    }
                }
                internal static class Misc
                {
                    internal static class Paths
                    {
                        public const string
                            GetGameDirectory = "SIOBOX_Paths_GetGameDirectory",
                            GetLogDirectory = "SIOBOX_Paths_GetLogDirectory",
                            GetAddonsDirectory = "SIOBOX_Paths_GetAddonsDirectory",
                            GetPluginsDirectory = "SIOBOX_Paths_GetPluginsDirectory",
                            GetSavesDirectory = "SIOBOX_Paths_GetSavesDirectory",
                            GetLaunchDirectory = "SIOBOX_Paths_GetLaunchDirectory",
                            GetCombine = "SIOBOX_Paths_GetCombine";
                    }

                    internal static class Debug {
                        public const string
                            EngineError = "SIOBOX_Engine_EngineError",
                            EngineLog = "SIOBOX_Engine_EngineLog";
                    }
                }
            }
        }
    }
}
