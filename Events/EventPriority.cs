﻿using System;
using System.Collections.Generic;
using System.Linq;
using SioBoxAPI.Addons;

namespace SioBoxAPI.Events
{
    public struct EventPriority : IComparable<EventPriority> {

        /// <summary>
        /// The Priority
        /// </summary>
        public int Priority;

        /// <summary>
        /// True if is created by default
        /// </summary>
        public bool Default;

        /// <summary>
        /// Call before these addons
        /// </summary>
        private AddonInfo[] m_beforeAddons, m_afterAddons;

        public EventPriority(int priority = 3, bool Default = true)
        {
            Priority = priority;
            this.Default = Default;
            m_beforeAddons = new AddonInfo[0];
            m_afterAddons = new AddonInfo[0];
        }
        
        /// <summary>
        /// This is called last
        /// </summary>
        public static readonly EventPriority LOW = new EventPriority(){Priority = 0, Default = false};
        /// <summary>
        /// This is called before last
        /// </summary>
        public static readonly EventPriority MEDIUM = new EventPriority(){Priority = 1, Default = false };
        /// <summary>
        /// This is called second
        /// </summary>
        public static readonly EventPriority HIGH = new EventPriority(){Priority = 2, Default = false };
        /// <summary>
        /// This is called first
        /// </summary>
        public static readonly EventPriority CRITICAL = new EventPriority(){Priority = 3, Default = false };

        /// <summary>
        /// Execute before these addons
        /// </summary>
        /// <param name="addons"></param>
        /// <returns></returns>
        public static EventPriority BEFORE(params AddonBase[] addons) {
            return BEFORE(addons.Select(add => add.AddonInfo).ToArray());
        }

        /// <summary>
        /// Execute before these addons
        /// </summary>
        /// <param name="addons"></param>
        /// <returns></returns>
        public static EventPriority BEFORE(params AddonInfo[] addons)
        {
            return new EventPriority(-1, false) { m_beforeAddons = addons };
        }

        /// <summary>
        /// Execute after these addons
        /// </summary>
        /// <param name="addons"></param>
        /// <returns></returns>
        public static EventPriority AFTER(params AddonBase[] addons)
        {
            return AFTER(addons.Select(add => add.AddonInfo).ToArray());
        }

        /// <summary>
        /// Execute after these addons
        /// </summary>
        /// <param name="addons"></param>
        /// <returns></returns>
        public static EventPriority AFTER(params AddonInfo[] addons)
        {
            return new EventPriority(-1, false) { m_afterAddons = addons };
        }

        /// <summary>
        /// Execute before these and after these addons
        /// </summary>
        /// <param name="beforeAddons"></param>
        /// <param name="afterAddons"></param>
        /// <returns></returns>
        public static EventPriority BEFORE_AFTER(AddonBase[] beforeAddons, AddonBase[] afterAddons)
        {
            return BEFORE_AFTER(beforeAddons.Select(add => add.AddonInfo).ToArray(), afterAddons.Select(add => add.AddonInfo).ToArray());
        }

        /// <summary>
        /// Execute before these and after these addons
        /// </summary>
        /// <param name="beforeAddons"></param>
        /// <param name="afterAddons"></param>
        /// <returns></returns>
        public static EventPriority BEFORE_AFTER(AddonInfo[] beforeAddons, AddonInfo[] afterAddons)
        {
            return new EventPriority(-1, false) { m_beforeAddons = beforeAddons, m_afterAddons = afterAddons};
        }

        /// <summary>
        /// Returns list with addons to load after this
        /// </summary>
        /// <param name="before"></param>
        public void GetBefore(out AddonInfo[] before) => before = m_beforeAddons;

        /// <summary>
        /// Returns list with addons to load before this
        /// </summary>
        /// <param name="after"></param>
        public void GetAfter(out AddonInfo[] after) => after = m_afterAddons;

        /// <summary>
        /// Get EventPriority by string name
        /// </summary>
        /// <param name="priority"></param>
        /// <returns></returns>
        public static EventPriority TryParse(string priority) {
            if (priority == "LOW") return LOW;
            if (priority == "MEDIUM") return MEDIUM;
            if (priority == "HIGH") return HIGH;
            if (priority == "CRITICAL") return CRITICAL;
            return default(EventPriority);
        }

        /// <summary>
        /// Keys
        /// </summary>
        public static string[] Keys => new string[4] {"LOW", "MEDIUM", "HIGH", "CRITICAL"};
        
        /// <summary>
        /// Values
        /// </summary>
        public static EventPriority[] Values => new EventPriority[4]{LOW, MEDIUM, HIGH, CRITICAL};

        /// <summary>
        /// Returns index in array of priority or -1 if not found
        /// </summary>
        /// <param name="priority"></param>
        /// <returns></returns>
        public static int IndexOf(EventPriority priority) {
            if (priority == LOW) return 0;
            if (priority == MEDIUM) return 1;
            if (priority == HIGH) return 2;
            if (priority == CRITICAL) return 3;
            return -1;
        }

        /// <summary>
        /// Returns the priority by index or default(EventPriority) -- priority.Default == true
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public static EventPriority FromIndex(int index) {
            if (index == 0) return LOW;
            if (index == 1) return MEDIUM;
            if (index == 2) return HIGH;
            if (index == 3) return CRITICAL;
            return default(EventPriority);
        }

        public static bool operator ==(EventPriority left, EventPriority right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(EventPriority left, EventPriority right)
        {
            return !left.Equals(right);
        }

        public bool Equals(EventPriority other) {
            return Priority == other.Priority && Default == other.Default && Equals(m_beforeAddons, other.m_beforeAddons) && Equals(m_afterAddons, other.m_afterAddons);
        }

        public override bool Equals(object obj) {
            if (ReferenceEquals(null, obj)) return false;
            return obj is EventPriority && Equals((EventPriority) obj);
        }

        public override int GetHashCode() {
            unchecked {
                var hashCode = Priority;
                hashCode = (hashCode * 397) ^ Default.GetHashCode();
                hashCode = (hashCode * 397) ^ (m_beforeAddons != null ? m_beforeAddons.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (m_afterAddons != null ? m_afterAddons.GetHashCode() : 0);
                return hashCode;
            }
        }

        private sealed class EventPriorityEqualityComparer : IEqualityComparer<EventPriority> {
            public bool Equals(EventPriority x, EventPriority y) {
                return x.Priority == y.Priority && x.Default == y.Default && Equals(x.m_beforeAddons, y.m_beforeAddons) && Equals(x.m_afterAddons, y.m_afterAddons);
            }

            public int GetHashCode(EventPriority obj) {
                unchecked {
                    var hashCode = obj.Priority;
                    hashCode = (hashCode * 397) ^ obj.Default.GetHashCode();
                    hashCode = (hashCode * 397) ^ (obj.m_beforeAddons != null ? obj.m_beforeAddons.GetHashCode() : 0);
                    hashCode = (hashCode * 397) ^ (obj.m_afterAddons != null ? obj.m_afterAddons.GetHashCode() : 0);
                    return hashCode;
                }
            }
        }

        public static IEqualityComparer<EventPriority> EventPriorityComparer { get; } = new EventPriorityEqualityComparer();

        public int CompareTo(EventPriority other) {
            var priorityComparison = Priority.CompareTo(other.Priority);
            if (priorityComparison != 0) return priorityComparison;
            return Default.CompareTo(other.Default);
        }

        private sealed class PriorityDefaultRelationalComparer : Comparer<EventPriority> {
            public override int Compare(EventPriority x, EventPriority y) {
                var priorityComparison = x.Priority.CompareTo(y.Priority);
                if (priorityComparison != 0) return priorityComparison;
                return x.Default.CompareTo(y.Default);
            }
        }

        public static Comparer<EventPriority> PriorityDefaultComparer { get; } = new PriorityDefaultRelationalComparer();

        public override string ToString() {
            return $"{nameof(Priority)}: {Priority}, {nameof(Default)}: {Default}, {nameof(m_beforeAddons)}: {String.Join(",", m_beforeAddons.Select(ad => ad.Id).ToArray())}, {nameof(m_afterAddons)}: {String.Join(",", m_afterAddons.Select(ad => ad.Id).ToArray())}";
        }
        
    }


    
}
