﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SioBoxAPI.Addons;
using SioBoxAPI.Events.Levels;

namespace SioBoxAPI.Events
{
    public class Events
    {
        [Obsolete("This is a future feature what is not yet.", true)]
        public static void BindMany(AddonBase addon, object classListener) {
            if (classListener == null) return;
            var clazz = classListener.GetType();
            var methods = clazz.GetMethods();
            foreach (var methodInfo in methods) {
                if (methodInfo.Name.StartsWith("_")) {
                    var parameters = methodInfo.GetParameters();
                    if (parameters.Length > 0) {
                        var firstParam = parameters[0];
                        var parameterType = firstParam.ParameterType;
                        EventPriority? priority = null;
                        if (parameters.Length > 1) {
                            var secondParam = parameters[1];
                            if (secondParam.ParameterType == typeof(EventPriority)) {
                                if (secondParam.HasDefaultValue) {
                                    priority = (EventPriority) secondParam.DefaultValue;
                                }
                            }
                        }

                        priority = priority ?? EventPriority.MEDIUM;

                        if (IsAssignableToGenericType(parameterType, typeof(BaseEvent<>))) {
                            var method = parameterType.GetMethod("Bind", new Type[] {typeof(AddonBase), typeof(Action), typeof(EventPriority)});
                            var action = Action.CreateDelegate(parameterType, methodInfo);

                            method.Invoke(null, new object[] {addon, action, priority});
                        }
                    }
                }
            }
        }

        [Obsolete("This is a future feature what is not yet.", true)]
        public static void UnbindMany(object classListener) {
            
        }

        private static bool IsAssignableToGenericType(Type givenType, Type genericType)
        {
            var interfaceTypes = givenType.GetInterfaces();

            foreach (var it in interfaceTypes)
            {
                if (it.IsGenericType && it.GetGenericTypeDefinition() == genericType)
                    return true;
            }

            if (givenType.IsGenericType && givenType.GetGenericTypeDefinition() == genericType)
                return true;

            Type baseType = givenType.BaseType;
            if (baseType == null) return false;

            return IsAssignableToGenericType(baseType, genericType);
        }
    }
}
