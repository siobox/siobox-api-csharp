﻿using System;
using System.Collections.Generic;
using SioBoxAPI.Addons;

namespace SioBoxAPI.Events
{
    public abstract class BaseEvent<TEvent> where TEvent : BaseEvent<TEvent> {
        protected static Dictionary<AddonInfo, List<Action<TEvent>>> m_delegates = new Dictionary<AddonInfo, List<Action<TEvent>>>();

        public static void Bind(AddonBase addonBase, Action<TEvent> OnEvent, EventPriority Priority = default(EventPriority)) {
            Bind(addonBase.AddonInfo, OnEvent, Priority);
        }

        public static void Bind(AddonInfo addonInfo, Action<TEvent> OnEvent, EventPriority Priority = default(EventPriority))
        {
            if(!m_delegates.ContainsKey(addonInfo)) m_delegates.Add(addonInfo, new List<Action<TEvent>>());
            m_delegates[addonInfo].Add(OnEvent);
        }

        public static void Unbind(AddonBase addonBase, Action<TEvent> action) {
            Unbind(addonBase.AddonInfo, action);
        }

        public static void Unbind(AddonInfo addonInfo, Action<TEvent> action) {
            m_delegates[addonInfo].Remove(action);
        }

        internal static void Execute(TEvent eEvent) {
            foreach (KeyValuePair<AddonInfo, List<Action<TEvent>>> keyValuePair in m_delegates) {
                
                // This condition cancel execution for addon if addon is not in ENABLED state.
                if (keyValuePair.Key.State.State != AddonState.StateEnum.ENABLED) continue;
                    keyValuePair.Value.ForEach(action => action(eEvent));
            }
        }
    }
}
