﻿using SioBoxAPI.Runtime.Engine;

namespace SioBoxAPI.Events.Levels
{
    public class LevelChangeEvent : CancellableBaseEvent<LevelChangeEvent> {
        public WorldInfo From, To;

        public LevelChangeEvent(WorldInfo from, WorldInfo to)
        {
            From = from;
            To = to;
        }
    }
}
