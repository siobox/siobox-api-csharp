﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SioBoxAPI.Events
{
    public abstract class CancellableBaseEvent<TEvent> : BaseEvent<TEvent>, ICancellable where TEvent : BaseEvent<TEvent> {
        public bool Cancelled { get; set; }
    }
}